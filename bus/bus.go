package bus

import (
	"time"

	log "github.com/sirupsen/logrus"
	czmq "github.com/zeromq/goczmq"
)

type Bus struct {
	name     string
	unit     string
	backend  string
	frontend string
	capture  string
}

func NewBus(name, unit, backend, frontend, capture string) *Bus {
	return &Bus{
		name,
		unit,
		backend,
		frontend,
		capture,
	}
}

// FIXME: This is currently here to test keeping the socket alive
func (b *Bus) captureThread(done chan bool) {
	pipe, _ := czmq.NewPull(b.capture)

	log.WithFields(log.Fields{"bus": b.name}).Info("capture proxy messages")
	for {
		msg, err := pipe.RecvMessage()
		if err != nil {
			break // Interrupted
		}
		log.WithFields(log.Fields{"bus": b.name}).Tracef("received: %s", msg)
	}

	log.WithFields(log.Fields{"bus": b.name}).Info("capture ended")
	done <- true
}

func (b *Bus) Run(done chan bool) {
	doneCapture := make(chan bool, 1)

	go b.captureThread(doneCapture)

	time.Sleep(100 * time.Millisecond)
	proxy := czmq.NewProxy()
	defer proxy.Destroy()

	if err := proxy.SetFrontend(czmq.XSub, b.frontend); err != nil {
		log.WithFields(log.Fields{
			"bus":      b.name,
			"endpoint": b.frontend,
		}).Error("failed to connect frontend to proxy")
		done <- true
	}
	log.WithFields(log.Fields{
		"bus":      b.name,
		"endpoint": b.frontend,
	}).Info("frontend connected")

	if err := proxy.SetBackend(czmq.XPub, b.backend); err != nil {
		log.WithFields(log.Fields{
			"bus":      b.name,
			"endpoint": b.backend,
		}).Error("failed to connect backend to proxy")
		done <- true
	}
	log.WithFields(log.Fields{
		"bus":      b.name,
		"endpoint": b.backend,
	}).Info("backend connected")

	if err := proxy.SetCapture(b.capture); err != nil {
		log.WithFields(log.Fields{
			"bus":      b.name,
			"endpoint": b.capture,
		}).Error("failed to connect capture to proxy")
		done <- true
	}
	log.WithFields(log.Fields{
		"bus":      b.name,
		"endpoint": b.capture,
	}).Info("capture connected")

	<-doneCapture

	log.WithFields(log.Fields{"bus": b.name}).Info("proxy stopped")
	done <- true
}
