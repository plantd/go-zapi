module gitlab.com/plantd/go-zapi

go 1.12

require (
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/pebbe/zmq4 v1.0.0
	github.com/sirupsen/logrus v1.4.2
	github.com/zeromq/goczmq v4.1.0+incompatible
)
