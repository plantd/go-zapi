# ZeroMQ Devices

[![Build Status](https://gitlab.com/plantd/go-zapi/badges/master/build.svg)](https://gitlab.com/plantd/go-zapi/commits/master)
[![Coverage Report](https://gitlab.com/plantd/go-zapi/badges/master/coverage.svg)](https://gitlab.com/plantd/go-zapi/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/go-zapi)](https://goreportcard.com/report/gitlab.com/plantd/go-zapi)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

ZeroMQ devices used with `plantd` services.

## Majordomo Protocol

* Broker
* Client
* Worker

Examples are available in the `examples/` directory and can be built with

```sh
go build -o bin/broker ./examples/broker.go
go build -o bin/client ./examples/client.go
go build -o bin/worker ./examples/worker.go
```

## PUBSUB

* Bus
* Source
* Sink
