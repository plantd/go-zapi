package mdp

import (
	//"errors"
	"testing"
)

func TestBroker(t *testing.T) {
	broker, err := NewBroker()
	if err != nil {
		t.Error("failed to create broker instance")
	}

	endpoint := "tcp://127.0.0.1:12345"
	if err = broker.Bind(endpoint); err != nil {
		t.Error("failed to bind to endpoint", endpoint)
	}

	done := make(chan bool, 1)
	go broker.Run(done)
	<-done

	if err = broker.Close(); err != nil {
		t.Error("failed to close broker endpoint connection")
	}
}
