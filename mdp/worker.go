// Majordomo Protocol Worker API.
// Implements the MDP/Worker spec at http://rfc.zeromq.org/spec:7.
package mdp

import (
	"runtime"
	"time"

	"gitlab.com/plantd/go-zapi/util"

	zmq "github.com/pebbe/zmq4"
)

const (
	heartbeat_liveness = 3 //3-5 is reasonable
)

// This is the structure of a worker API instance. We use a pseudo-OO
// approach in a lot of the C examples, as well as the CZMQ binding:

// Structure of our class
// We access these properties only via class methods

// Majordomo Protocol Worker API.
type Worker struct {
	broker  string
	service string
	worker  *zmq.Socket // Socket to broker
	poller  *zmq.Poller

	// Heartbeat management
	heartbeat_at time.Time     // When to send HEARTBEAT
	liveness     int           // How many attempts left
	heartbeat    time.Duration // Heartbeat delay, msecs
	reconnect    time.Duration // Reconnect delay, msecs

	expect_reply bool   // False only at start
	reply_to     string // Return identity, if any
}

// We have two utility functions; to send a message to the broker and
// to (re-)connect to the broker:

// ---------------------------------------------------------------------

// Send message to broker.
func (w *Worker) SendToBroker(command string, option string, msg []string) (err error) {
	n := 3
	if option != "" {
		n++
	}
	m := make([]string, n, n+len(msg))
	m = append(m, msg...)

	// Stack protocol envelope to start of message
	if option != "" {
		m[3] = option
	}
	m[2] = command
	m[1] = MDPW_WORKER
	m[0] = ""

	_, err = w.worker.SendMessage(m)
	return
}

// ---------------------------------------------------------------------

// Connect or reconnect to broker.
func (w *Worker) ConnectToBroker() (err error) {
	if w.worker != nil {
		w.worker.Close()
		w.worker = nil
	}

	w.worker, err = zmq.NewSocket(zmq.DEALER)
	err = w.worker.Connect(w.broker)
	w.poller = zmq.NewPoller()
	w.poller.Add(w.worker, zmq.POLLIN)

	// Register service with broker
	err = w.SendToBroker(MDPW_READY, w.service, []string{})

	// If liveness hits zero, queue is considered disconnected
	w.liveness = heartbeat_liveness
	w.heartbeat_at = time.Now().Add(w.heartbeat)

	return
}

// Here we have the constructor and destructor for our w class:

// ---------------------------------------------------------------------
// Constructor

func NewWorker(broker, service string) (w *Worker, err error) {

	w = &Worker{
		broker:    broker,
		service:   service,
		heartbeat: 2500 * time.Millisecond,
		reconnect: 2500 * time.Millisecond,
	}

	err = w.ConnectToBroker()

	runtime.SetFinalizer(w, (*Worker).Close)

	return
}

// ---------------------------------------------------------------------
// Destructor

func (w *Worker) Close() {
	if w.worker != nil {
		w.worker.Close()
		w.worker = nil
	}
}

// We provide two methods to configure the worker API. You can set the
// heartbeat interval and retries to match the expected network performance.

// ---------------------------------------------------------------------

// Set heartbeat delay.
func (w *Worker) SetHeartbeat(heartbeat time.Duration) {
	w.heartbeat = heartbeat
}

// ---------------------------------------------------------------------

// Set reconnect delay.
func (w *Worker) SetReconnect(reconnect time.Duration) {
	w.reconnect = reconnect
}

// This is the recv method; it's a little misnamed since it first sends
// any reply and then waits for a new request. If you have a better name
// for this, let me know:

// ---------------------------------------------------------------------

// Send reply, if any, to broker and wait for next request.
func (w *Worker) Recv(reply []string) (msg []string, err error) {
	// Format and send the reply if we were provided one
	if len(reply) == 0 && w.expect_reply {
	}

	if len(reply) > 0 {
		if w.reply_to == "" {
		}

		m := make([]string, 2, 2+len(reply))
		m = append(m, reply...)
		m[0] = w.reply_to
		m[1] = ""
		err = w.SendToBroker(MDPW_REPLY, "", m)
	}

	w.expect_reply = true

	for {
		var polled []zmq.Polled
		polled, err = w.poller.Poll(w.heartbeat)

		if err != nil {
			break // Interrupted
		}

		if len(polled) > 0 {
			msg, err = w.worker.RecvMessage(0)
			if err != nil {
				break // Interrupted
			}

			w.liveness = heartbeat_liveness

			// Don't try to handle errors, just assert noisily
			if len(msg) < 3 {
			}

			if msg[0] != "" {
			}

			if msg[1] != MDPW_WORKER {
			}

			command := msg[2]
			msg = msg[3:]
			switch command {
			case MDPW_REQUEST:
				// We should pop and save as many addresses as there are
				// up to a null part, but for now, just save one...
				w.reply_to, msg = util.Unwrap(msg)
				// Here is where we actually have a message to process; we
				// return it to the caller application:
				return // We have a request to process
			case MDPW_HEARTBEAT:
				// Do nothing for heartbeats
			case MDPW_DISCONNECT:
				w.ConnectToBroker()
			default:
			}
		} else {
			w.liveness--
			if w.liveness == 0 {
				time.Sleep(w.reconnect)
				w.ConnectToBroker()
			}
		}
		// Send HEARTBEAT if it's time
		if time.Now().After(w.heartbeat_at) {
			w.SendToBroker(MDPW_HEARTBEAT, "", []string{})
			w.heartbeat_at = time.Now().Add(w.heartbeat)
		}
	}
	return
}
