package main

import (
	"log"

	"gitlab.com/plantd/go-zapi/mdp"
)

func main() {
	broker, err := mdp.NewBroker()
	if err != nil {
		log.Println("failed to create broker instance")
	}

	endpoint := "tcp://127.0.0.1:5555"
	if err = broker.Bind(endpoint); err != nil {
		log.Println("failed to bind to endpoint", endpoint)
	}

	done := make(chan bool, 1)
	go broker.Run(done)
	<-done

	if err = broker.Close(); err != nil {
		log.Println("failed to close broker endpoint connection")
	}
}
