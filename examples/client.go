package main

import (
	"log"

	"gitlab.com/plantd/go-zapi/mdp"
)

func main() {
	client, _ := mdp.NewClient("tcp://localhost:5555")

	var count int
	for count = 0; count < 100; count++ {
		err := client.Send("echo", "ping")
		if err != nil {
			log.Println("Send:", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		msg, err := client.Recv()
		if err != nil {
			log.Println("Recv:", err)
			break
		}
		log.Println(msg)
	}

	log.Printf("%d replies received\n", count)
}
