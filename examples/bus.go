package main

import (
	"log"

	"gitlab.com/plantd/go-zapi/bus"
)

func main() {
	bus := bus.NewBus(
		"test",
		"foo",
		"@tcp://127.0.0.1:9200",
		"@tcp://127.0.0.1:9201",
		"inproc://foo.test.pipe",
	)
	done := make(chan bool)

	go bus.Run(done)

	<-done

	log.Print("closing bus example")
}
