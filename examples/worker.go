package main

import (
	"log"

	"gitlab.com/plantd/go-zapi/mdp"
)

func main() {
	worker, _ := mdp.NewWorker("tcp://localhost:5555", "echo")

	var err error
	var request, reply []string
	for {
		request, err = worker.Recv(reply)
		if err != nil {
			break
		}
		reply = request
	}

	log.Println(err)
}
